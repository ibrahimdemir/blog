CREATE DATABASE blogdb
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;
  
CREATE USER 'blogdb'@'localhost' IDENTIFIED BY 'p4ssw0rd';

GRANT SELECT,INSERT,UPDATE,DELETE
   ON blogdb.*
   TO 'blogdb'@'localhost';

USE blogdb;

CREATE TABLE users (
  id       int(11)     NOT NULL AUTO_INCREMENT,
  username varchar(50) NOT NULL,
  password varchar(50) NOT NULL,
  fullname varchar(50) NOT NULL,
  role     varchar(20) NOT NULL,
  isactive  tinyint(4)  DEFAULT NULL,
  PRIMARY KEY (id)
)  DEFAULT CHARSET=utf8 ;


CREATE TABLE entries (
  id             int(11)     NOT NULL AUTO_INCREMENT,
  title          varchar(255) NOT NULL,
  body           text        DEFAULT NULL,
  author_id      int(11)     NOT NULL,
  create_date    datetime    NOT NULL,
  publish_date   datetime    DEFAULT NULL,
  publishedby_id int(11)     DEFAULT NULL,
  status         varchar(20) NOT NULL,
  PRIMARY KEY (id)
)  DEFAULT CHARSET=utf8 ;

INSERT INTO users (username,password,fullname,role,isactive)
VALUES ('admin','admin','admin','ADMIN',1);



