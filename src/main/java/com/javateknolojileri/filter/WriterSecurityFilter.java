package com.javateknolojileri.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.javateknolojileri.controller.LoginBean;

@WebFilter("/faces/writer/*")
public class WriterSecurityFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		if (!isLoggedIn(request)) {
			((HttpServletResponse) response)
					.sendRedirect(((HttpServletRequest) request)
							.getContextPath() + "/faces/login.xhtml");
			return;
		}
		// writer editor yada admin rolleri login olduktan
		// sonra writer klasörüne erişim hakkına sahiptir.
		// Bu nedenle writer klasörü için rol kontrolüne gerek yoktur.
		chain.doFilter(request, response);

	}

	private boolean isLoggedIn(ServletRequest request) {
		HttpSession session = ((HttpServletRequest) request).getSession();
		return session.getAttribute(LoginBean.SESSION_USER) != null;
	}

	@Override
	public void init(FilterConfig config) throws ServletException {

	}

}
