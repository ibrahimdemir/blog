package com.javateknolojileri.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.javateknolojileri.controller.LoginBean;
import com.javateknolojileri.model.User;
import com.javateknolojileri.model.UserRole;

@WebFilter("/faces/admin/*")
public class AdminSecurityFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		if (!isLoggedIn(request)) {
			((HttpServletResponse) response)
					.sendRedirect(((HttpServletRequest) request)
							.getContextPath() + "/faces/login.xhtml");
			return;
		}
		if (!isAdmin(request)) {
			((HttpServletResponse) response)
					.sendRedirect(((HttpServletRequest) request)
							.getContextPath() + "/faces/restricted.xhtml");
			return;
		}
		chain.doFilter(request, response);
	}

	private boolean isLoggedIn(ServletRequest request) {
		HttpSession session = ((HttpServletRequest) request).getSession();
		User user = (User) session.getAttribute(LoginBean.SESSION_USER);
		return user != null;
	}

	private boolean isAdmin(ServletRequest request) {
		HttpSession session = ((HttpServletRequest) request).getSession();
		User user = (User) session.getAttribute(LoginBean.SESSION_USER);
		return user != null && user.getRole() == UserRole.ADMIN;
	}

	@Override
	public void init(FilterConfig config) throws ServletException {

	}

}
