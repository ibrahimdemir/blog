package com.javateknolojileri.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.javateknolojileri.model.Entry;
import com.javateknolojileri.model.EntryStatus;
import com.javateknolojileri.model.User;
import com.javateknolojileri.model.UserRole;

public class EntryDAO extends BaseDAO implements Serializable {
	private static final String baseQuery = "SELECT e.id            AS entry_id"
			+ ", e.title         AS entry_title"
			+ ", e.body          AS entry_body"
			+ ", e.create_date   AS entry_create_date"
			+ ", e.publish_date  AS entry_publish_date"
			+ ", e.status        AS entry_status"
			+ ", a.id            AS author_id"
			+ ", a.username      AS author_username"
			+ ", a.password      AS author_password"
			+ ", a.fullname      AS author_fullname"
			+ ", a.role          AS author_role"
			+ ", a.isactive      AS author_isactive"
			+ ", p.id            AS publishedby_id"
			+ ", p.username      AS publishedby_username"
			+ ", p.password      AS publishedby_password"
			+ ", p.fullname      AS publishedby_fullname"
			+ ", p.role          AS publishedby_role"
			+ ", p.isactive      AS publishedby_isactive"
			+ " FROM entries AS e"
			+ " INNER JOIN users AS a"
			+ " ON e.author_id = a.id"
			+ " LEFT OUTER JOIN users AS p"
			+ " ON e.publishedby_id = p.id";

	public EntryDAO(Connection connection) {
		super(connection);
	}

	private List<Entry> getData(String query, Object... parameters) {
		List<Entry> list = new ArrayList<Entry>();
		List<Map<String, Object>> result = this.executeQuery(query, parameters);
		for (Map<String, Object> row : result) {

			/* set author */
			User author = new User();
			author.setId((int) row.get("author_id"));
			author.setUsername((String) row.get("author_username"));
			author.setPassword((String) row.get("author_password"));
			author.setFullname((String) row.get("author_fullname"));
			author.setRole(UserRole.valueOf((String) row.get("author_role")));
			author.setActive(((int) row.get("author_isactive")) == 1);
			/* set publishedBy */
			User publishedBy = null;
			if (row.get("publishedby_id") != null) {
				publishedBy = new User();
				publishedBy.setId((int) row.get("publishedby_id"));
				publishedBy.setUsername((String) row
						.get("publishedby_username"));
				publishedBy.setPassword((String) row
						.get("publishedby_password"));
				publishedBy.setFullname((String) row
						.get("publishedby_fullname"));
				publishedBy.setRole(UserRole.valueOf((String) row
						.get("publishedby_role")));
				publishedBy
						.setActive(((int) row.get("publishedby_isactive")) == 1);
			}
			/* set entry */
			Entry model = new Entry();
			model.setId((int) row.get("entry_id"));
			model.setTitle((String) row.get("entry_title"));
			model.setBody((String) row.get("entry_body"));
			model.setAuthor(author);
			model.setCreateDate((Date) row.get("entry_create_date"));
			model.setPublishDate((Date) row.get("entry_publish_date"));
			model.setPublishedBy(publishedBy);
			model.setStatus(EntryStatus.valueOf((String) row
					.get("entry_status")));
			list.add(model);
		}
		return list;
	}

	public List<Entry> getList() {
		String query = baseQuery;
		return this.getData(query);
	}

	public List<Entry> getListByWriter(int authorId) {
		String query = baseQuery + " Where e.author_id = ?";
		return this.getData(query, authorId);
	}

	public List<Entry> getListByStatus(EntryStatus status) {
		String query = baseQuery + " Where e.status = ?";
		return this.getData(query, status.toString());
	}

	public Entry getEntryById(int id) {
		String query = baseQuery + " Where e.id = ?";
		List<Entry> entryList = this.getData(query, id);
		if (entryList.size() > 0)
			return entryList.get(0);
		else
			return null;
	}

	public void insert(Entry entry) {
		Integer publishedById = entry.getPublishedBy() == null ? null : entry
				.getPublishedBy().getId();
		String query = "INSERT INTO entries (title, body, author_id"
				+ " , create_date, publish_date, publishedby_id, status) "
				+ " VALUES(?, ?, ?, ?, ?, ?, ?)";
		System.out.println(entry);
		Object[] parameters = { entry.getTitle(), entry.getBody(),
				entry.getAuthor().getId(), entry.getCreateDate(),
				entry.getPublishDate(), publishedById,
				entry.getStatus().toString() };
		this.execute(query, parameters);
	}

	public void update(Entry entry) {
		Integer publishedById = entry.getPublishedBy() == null ? null : entry
				.getPublishedBy().getId();
		String query = "UPDATE entries SET title = ?, body = ?, author_id = ?, create_date = ?,"
				+ " publish_date = ?, publishedby_id = ?, status = ? WHERE id = ?";

		Object[] parameters = { entry.getTitle(), entry.getBody(),
				entry.getAuthor().getId(), entry.getCreateDate(),
				entry.getPublishDate(), publishedById,
				entry.getStatus().toString(), entry.getId() };
		this.execute(query, parameters);
	}

}
