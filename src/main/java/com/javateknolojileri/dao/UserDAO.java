package com.javateknolojileri.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.javateknolojileri.model.User;
import com.javateknolojileri.model.UserRole;

public class UserDAO extends BaseDAO implements Serializable {

	public UserDAO(Connection connection) {
		super(connection);
	}

	private List<User> getData(String query, Object... parameters) {
		List<User> list = new ArrayList<User>();
		List<Map<String, Object>> result = this.executeQuery(query, parameters);
		for (Map<String, Object> row : result) {
			User model = new User();
			model.setId((int) row.get("id"));
			model.setUsername((String) row.get("username"));
			model.setPassword((String) row.get("password"));
			model.setFullname((String) row.get("fullname"));
			model.setRole(UserRole.valueOf((String) row.get("role")));
			model.setActive(((int) row.get("isactive")) == 1);
			list.add(model);
		}
		return list;
	}

	public List<User> getList() {
		String query = "Select * From users";
		return this.getData(query);
	}

	public User getUserById(int id) {
		User user = null;
		String query = "Select * From users Where id = ?";
		List<User> userList = this.getData(query, id);
		if (userList.size() > 0)
			user = userList.get(0);
		return user;
	}
	public User getUserByUsername(String username) {
		User user = null;
		String query = "Select * From users Where username = ?";
		List<User> userList = this.getData(query, username);
		if (userList.size() > 0)
			user = userList.get(0);
		return user;
	}

	public void insert(User user) {
		String query = "INSERT INTO users (username, password, fullname, role, isactive) "
				+ " VALUES(?, ?, ?, ?, ?)";
		Object[] parameters = { user.getUsername(), user.getPassword(),
				user.getFullname(), user.getRole().toString(), user.isActive() };
		this.execute(query, parameters);
	}

	public void update(User user) {
		String query = "UPDATE users SET username = ?, password  = ?, fullname = ?, "
				+ "role = ?, isactive = ?  WHERE id = ?";

		Object[] parameters = { user.getUsername(), user.getPassword(),
				user.getFullname(), user.getRole().toString(), user.isActive(),
				user.getId() };
		this.execute(query, parameters);
	}

}
