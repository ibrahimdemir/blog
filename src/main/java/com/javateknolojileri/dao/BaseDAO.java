package com.javateknolojileri.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseDAO implements Serializable {
	private Connection connection;

	public BaseDAO(Connection connection) {
		this.connection = connection;
	}

	protected List<Map<String, Object>> executeQuery(String query,
			Object... parameters) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		try {
			PreparedStatement pstmt = createPreparedStatement(query, parameters);
			ResultSet rs = pstmt.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			while (rs.next()) {
				Map<String, Object> row = new HashMap<String, Object>();
				for (int i = 0; i < rsmd.getColumnCount(); i++) {
					String columnName = rsmd.getColumnLabel(i + 1);
					row.put(columnName, rs.getObject(columnName));
				}
				result.add(row);
			}
			rs.close();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	protected void execute(String query, Object... parameters) {
		try {
			PreparedStatement pstmt = createPreparedStatement(query, parameters);
			pstmt.execute();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private PreparedStatement createPreparedStatement(String query,
			Object... parameters) {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(query);
			if (parameters != null) {
				for (int i = 0; i < parameters.length; i++) {
					preparedStatement.setObject(i + 1, parameters[i]);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return preparedStatement;
	}

}
