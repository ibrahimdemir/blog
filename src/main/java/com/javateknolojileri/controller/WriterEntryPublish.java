package com.javateknolojileri.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.javateknolojileri.model.Entry;
import com.javateknolojileri.model.EntryStatus;
import com.javateknolojileri.service.EntryService;
import com.javateknolojileri.util.FacesUtil;

@ManagedBean(name = "writerEntryPublish")
@ViewScoped
public class WriterEntryPublish implements Serializable {

	private EntryService entryService = new EntryService();
	private Entry entry = new Entry();

	@PostConstruct
	private void initalize() {
		int id = Integer.valueOf(FacesUtil.getParamter("id"));
		entry = entryService.getEntryById(id);
	}

	public String save() {
		entry.setStatus(EntryStatus.WAITINGFORAPPROVE);
		entryService.update(entry);
		return "/writer/entry/list?faces-redirect=true";
	}

	public Entry getEntry() {
		return entry;
	}

	public void setEntry(Entry entry) {
		this.entry = entry;
	}

}
