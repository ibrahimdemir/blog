package com.javateknolojileri.controller;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.javateknolojileri.model.Entry;
import com.javateknolojileri.model.EntryStatus;
import com.javateknolojileri.model.User;
import com.javateknolojileri.service.EntryService;
import com.javateknolojileri.util.FacesUtil;

@ManagedBean(name = "writerEntryCreate")
@RequestScoped
public class WriterEntryCreateBean implements Serializable {

	private EntryService entryService = new EntryService();
	private Entry entry = new Entry();

	public String save() {
		entry.setAuthor((User) FacesUtil.getSession().getAttribute(
				LoginBean.SESSION_USER));
		entry.setCreateDate(new Date());
		entry.setStatus(EntryStatus.DRAFT);
		entryService.insert(entry);
		return "/writer/entry/list?faces-redirect=true";
	}

	public Entry getEntry() {
		return entry;
	}

	public void setEntry(Entry entry) {
		this.entry = entry;
	}

}
