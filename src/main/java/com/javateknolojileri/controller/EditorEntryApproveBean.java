package com.javateknolojileri.controller;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.javateknolojileri.model.Entry;
import com.javateknolojileri.model.EntryStatus;
import com.javateknolojileri.model.User;
import com.javateknolojileri.service.EntryService;
import com.javateknolojileri.util.FacesUtil;

@ManagedBean(name = "editorEntryApprove")
@ViewScoped
public class EditorEntryApproveBean implements Serializable {

	private EntryService entryService = new EntryService();
	private Entry entry = new Entry();

	@PostConstruct
	private void initalize() {
		int id = Integer.valueOf(FacesUtil.getParamter("id"));
		entry = entryService.getEntryById(id);
	}

	public String save() {
		User publishedBy = (User) FacesUtil.getSession().getAttribute(
				LoginBean.SESSION_USER);
		entry.setStatus(EntryStatus.PUBLISHED);
		entry.setPublishDate(new Date());
		entry.setPublishedBy(publishedBy);
		entryService.update(entry);
		return "/editor/entry/list?faces-redirect=true";
	}

	public Entry getEntry() {
		return entry;
	}

	public void setEntry(Entry entry) {
		this.entry = entry;
	}

}
