package com.javateknolojileri.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.javateknolojileri.model.Entry;
import com.javateknolojileri.service.EntryService;

@ManagedBean(name = "home")
@RequestScoped
public class HomeBean implements Serializable {

	private List<Entry> entryList;
	private EntryService entryService = new EntryService();

	@PostConstruct
	private void initalize() {
		entryList = entryService.getPublishedList();
	}

	public List<Entry> getEntryList() {
		return entryList;
	}

	public void setEntryList(List<Entry> entryList) {
		this.entryList = entryList;
	}

}
