package com.javateknolojileri.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.javateknolojileri.model.User;
import com.javateknolojileri.service.UserService;

@ManagedBean(name = "adminUserCreate")
@RequestScoped
public class AdminUserCreateBean implements Serializable {

	private UserService userService = new UserService();
	private User user = new User();

	public String save() {
		user.setActive(true);
		userService.insert(user);
		return "/admin/user/list?faces-redirect=true";
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
