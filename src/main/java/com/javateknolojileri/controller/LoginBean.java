package com.javateknolojileri.controller;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.javateknolojileri.model.User;
import com.javateknolojileri.model.UserRole;
import com.javateknolojileri.service.UserService;
import com.javateknolojileri.util.FacesUtil;

@ManagedBean(name = "login")
@SessionScoped
public class LoginBean implements Serializable {

	public static final String SESSION_USER = "SESSION_USER";

	private String username;
	private String password;
	private UserService userService = new UserService();

	public String doLogin() {
		if (username == null || username.trim().length() == 0) {
			FacesUtil.addMessage("Kullanıcı boş olamaz",
					FacesMessage.SEVERITY_ERROR);
			return null;
		}
		if (password == null || password.trim().length() == 0) {
			FacesUtil.addMessage("Şifre bol olamaz",
					FacesMessage.SEVERITY_ERROR);
			return null;
		}
		User user = userService.getUserByUsername(username);
		if (user == null || !password.equalsIgnoreCase(user.getPassword())) {
			FacesUtil.addMessage("Kullanıcı yada şifre hatalı",
					FacesMessage.SEVERITY_ERROR);
			return null;
		}

		FacesUtil.getSession().setAttribute(LoginBean.SESSION_USER, user);
		return "/home?faces-redirect=true";

	}

	public boolean isEditor() {
		User user = (User) FacesUtil.getSession().getAttribute(SESSION_USER);
		return (user != null && (user.getRole() == UserRole.EDITOR || user
				.getRole() == UserRole.ADMIN));
	}

	public boolean isAdmin() {
		User user = (User) FacesUtil.getSession().getAttribute(SESSION_USER);
		return (user != null && user.getRole() == UserRole.ADMIN);
	}

	public String doLogout() {
		FacesUtil.getSession().setAttribute(LoginBean.SESSION_USER, null);
		return "/home?faces-redirect=true";
	}

	public boolean isLogged() {
		return FacesUtil.getSession().getAttribute(SESSION_USER) != null;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
