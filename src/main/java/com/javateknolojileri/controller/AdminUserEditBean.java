package com.javateknolojileri.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.javateknolojileri.model.User;
import com.javateknolojileri.service.UserService;
import com.javateknolojileri.util.FacesUtil;

@ManagedBean(name = "adminUserEdit")
@ViewScoped
public class AdminUserEditBean implements Serializable {

	private UserService userService = new UserService();
	private User user = new User();

	@PostConstruct
	private void initalize() {
		int id = Integer.valueOf(FacesUtil.getParamter("id"));
		user = userService.getUserById(id);
	}

	public String save() {
		userService.update(user);
		return "/admin/user/list?faces-redirect=true";
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
