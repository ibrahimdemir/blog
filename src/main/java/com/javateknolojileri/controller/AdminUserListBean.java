package com.javateknolojileri.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.javateknolojileri.model.User;
import com.javateknolojileri.service.UserService;

@ManagedBean(name = "adminUserList")
@RequestScoped
public class AdminUserListBean implements Serializable {

	private UserService userService = new UserService();
	private List<User> userList = new ArrayList<User>();

	@PostConstruct
	private void initalize() {
		userList = userService.getUserList();
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

}
