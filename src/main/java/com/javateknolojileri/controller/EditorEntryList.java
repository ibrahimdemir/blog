package com.javateknolojileri.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.javateknolojileri.model.Entry;
import com.javateknolojileri.service.EntryService;

@ManagedBean(name = "editorEntryList")
@RequestScoped
public class EditorEntryList implements Serializable {

	private EntryService entryService = new EntryService();
	private List<Entry> entryList = new ArrayList<Entry>();

	@PostConstruct
	private void initalize() {
		entryList = entryService.getListForWaitingAprove();
	}

	public List<Entry> getEntryList() {
		return entryList;
	}

	public void setEntryList(List<Entry> entryList) {
		this.entryList = entryList;
	}

}
