package com.javateknolojileri.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.javateknolojileri.model.Entry;
import com.javateknolojileri.model.User;
import com.javateknolojileri.service.EntryService;
import com.javateknolojileri.util.FacesUtil;

@ManagedBean(name = "writerEntryList")
@RequestScoped
public class WriterEntryListBean implements Serializable {

	private EntryService entryService = new EntryService();
	private List<Entry> entryList = new ArrayList<Entry>();
	
	
	@PostConstruct
	private void initalize() {
		User author = (User) FacesUtil.getSession().getAttribute(LoginBean.SESSION_USER);
		entryList = entryService.getListByAuthor(author);
	}

	public List<Entry> getEntryList() {
		return entryList;
	}

	public void setEntryList(List<Entry> entryList) {
		this.entryList = entryList;
	}

}
