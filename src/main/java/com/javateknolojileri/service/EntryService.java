package com.javateknolojileri.service;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.javateknolojileri.dao.ConnectionFactory;
import com.javateknolojileri.dao.EntryDAO;
import com.javateknolojileri.model.Entry;
import com.javateknolojileri.model.EntryStatus;
import com.javateknolojileri.model.User;

public class EntryService implements Serializable {

	public List<Entry> getEntryList() {
		List<Entry> entryList = new ArrayList<Entry>();
		try {
			Connection connection = ConnectionFactory.getConnection();
			EntryDAO entryDAO = new EntryDAO(connection);
			entryList = entryDAO.getList();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entryList;
	}

	public List<Entry> getPublishedList() {
		List<Entry> entryList = new ArrayList<Entry>();
		try {
			Connection connection = ConnectionFactory.getConnection();
			EntryDAO entryDAO = new EntryDAO(connection);
			entryList = entryDAO.getListByStatus(EntryStatus.PUBLISHED);
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entryList;
	}
	
	public List<Entry> getListForWaitingAprove() {
		List<Entry> entryList = new ArrayList<Entry>();
		try {
			Connection connection = ConnectionFactory.getConnection();
			EntryDAO entryDAO = new EntryDAO(connection);
			entryList = entryDAO.getListByStatus(EntryStatus.WAITINGFORAPPROVE);
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entryList;
	}
	
	public List<Entry> getListByAuthor(User author) {
		List<Entry> entryList = new ArrayList<Entry>();
		try {
			Connection connection = ConnectionFactory.getConnection();
			EntryDAO entryDAO = new EntryDAO(connection);
			entryList = entryDAO.getListByWriter(author.getId());
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entryList;
	}

	public Entry getEntryById(int id) {
		Entry entry = null;
		try {
			Connection connection = ConnectionFactory.getConnection();
			EntryDAO entryDAO = new EntryDAO(connection);
			entry = entryDAO.getEntryById(id);
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return entry;
	}

	public void insert(Entry entry) {
		try {
			Connection connection = ConnectionFactory.getConnection();
			connection.setAutoCommit(false);
			EntryDAO entryDAO = new EntryDAO(connection);
			entryDAO.insert(entry);
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void update(Entry entry) {
		try {
			Connection connection = ConnectionFactory.getConnection();
			connection.setAutoCommit(false);
			EntryDAO entryDAO = new EntryDAO(connection);
			entryDAO.update(entry);
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
