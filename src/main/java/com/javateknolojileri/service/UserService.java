package com.javateknolojileri.service;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.javateknolojileri.dao.ConnectionFactory;
import com.javateknolojileri.dao.UserDAO;
import com.javateknolojileri.model.User;

public class UserService implements Serializable {

	public List<User> getUserList() {
		List<User> userList = new ArrayList<User>();
		try {
			Connection connection = ConnectionFactory.getConnection();
			UserDAO userDAO = new UserDAO(connection);
			userList = userDAO.getList();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}

	public User getUserByUsername(String username) {
		User user = null;
		try {
			Connection connection = ConnectionFactory.getConnection();
			UserDAO userDAO = new UserDAO(connection);
			user = userDAO.getUserByUsername(username);
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public User getUserById(int id) {
		User user = null;
		try {
			Connection connection = ConnectionFactory.getConnection();
			UserDAO userDAO = new UserDAO(connection);
			user = userDAO.getUserById(id);
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	public void insert(User user) {
		try {
			Connection connection = ConnectionFactory.getConnection();
			connection.setAutoCommit(false);
			UserDAO userDAO = new UserDAO(connection);
			userDAO.insert(user);
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void update(User user) {
		try {
			Connection connection = ConnectionFactory.getConnection();
			connection.setAutoCommit(false);
			UserDAO userDAO = new UserDAO(connection);
			userDAO.update(user);
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
