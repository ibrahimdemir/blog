package com.javateknolojileri.model;

public enum EntryStatus {

	DRAFT, PUBLISHED, WAITINGFORAPPROVE, DELETED;

}
