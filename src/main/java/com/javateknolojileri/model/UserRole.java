package com.javateknolojileri.model;

public enum UserRole {

	ADMIN, EDITOR, WRITER;

}
