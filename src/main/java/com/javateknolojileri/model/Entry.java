package com.javateknolojileri.model;

import java.io.Serializable;
import java.util.Date;

public class Entry implements Serializable {

	private int id;
	private String title;
	private String body;
	private User author;
	private Date createDate;
	private Date publishDate;
	private User publishedBy;
	private EntryStatus status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public User getPublishedBy() {
		return publishedBy;
	}

	public void setPublishedBy(User publishedBy) {
		this.publishedBy = publishedBy;
	}

	public EntryStatus getStatus() {
		return status;
	}

	public void setStatus(EntryStatus status) {
		this.status = status;
	}

	public boolean isDraft() {
		return this.status == EntryStatus.DRAFT;
	}

	public boolean isDeleted() {
		return this.status == EntryStatus.DELETED;
	}

	public boolean isPublised() {
		return this.status == EntryStatus.PUBLISHED;
	}

	public boolean isWaitingForApprove() {
		return this.status == EntryStatus.WAITINGFORAPPROVE;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entry other = (Entry) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Entry [id=" + id + ", title=" + title + ", body=" + body
				+ ", author=" + author + ", createDate=" + createDate
				+ ", publishDate=" + publishDate + ", publishedBy="
				+ publishedBy + ", status=" + status + "]";
	}

}
